import React, { useEffect } from "react";
import loadjs from "loadjs";

export default function ProductIndex() {
  console.log("hello from single product page");
  useEffect(() => {
    loadjs(
      [
        "js/bootstrap.js",
        "js/jquery.smartmenus.js",
        "js/jquery.smartmenus.bootstrap.js",
        "js/custom.js",
        "js/nouislider.js",

        "js/jquery.simpleLens.js",

        "js/jquery.simpleLens.js",
        "js/jquery.simpleGallery.js",
        // "js/sequence-theme.modern-slide-in.js",
        // "js/sequence.js",
      ],
      function () {
        /* foo.css, bar.png and thunk.js loaded */
      }
    );
  }, []);
  return <div>Single Product Page</div>;
}
