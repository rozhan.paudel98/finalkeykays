import Header from "../components/Header";
import Footer from "../components/Footer";

export default function AboutUs() {
  return (
    <>
      <Header />
      <div style={{ paddingBottom: "10vh" }}>
        <section id="aa-catg-head-banner">
          <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img" />
          <div className="aa-catg-head-banner-area">
            <div className="container">
              <div className="aa-catg-head-banner-content">
                <h2>About Us</h2>
                <ol className="breadcrumb">
                  <li>
                    <a href="index.html">Home</a>
                  </li>
                  <li className="active">Account</li>
                </ol>
              </div>
            </div>
          </div>
        </section>
      </div>

      <section>
        <div
          className="about-section"
          style={{
            display: " flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",

            height: "150vh",
            alignItem: "center",
            width: "70vw",
            textAlign: "left",
            marginLeft: "15vw",
            marginBottom: "10vh",
            position: "relative",
          }}
        >
          <h2>
            {" "}
            <p
              style={{
                lineHeight: "3rem",
              }}
            >
              {" "}
              We started as local digital goods solution in Nepal and also
              software support business company. Our company is registered in UK
              and our friendly staff are always ready to help you out. It’s very
              simple, you buy the product, get it instantly (most of the time)
              with our activation guide tutorials. Since, 2020 we have been
              serving various clients from all around the globe (most of them
              from Europe).{" "}
            </p>
          </h2>

          <h1 style={{ marginBottom: "1rem" }}>INSTANT DELIVERY</h1>
          <h2>
            {" "}
            <p
              style={{
                lineHeight: "3rem",
              }}
            >
              Our automated delivery system will provide your Game/Software code
              instantly. However, in case of stock out we deliver within few
              hours in your email.
            </p>
          </h2>

          <h1 style={{ marginBottom: "1rem" }}>MONEY BACK GUARANTEE</h1>
          <h2>
            {" "}
            <p
              style={{
                lineHeight: "3rem",
              }}
            >
              We are very sure about our product and we trust all our supplier.
              In case if we can’t provide you with a working product, we will
              give you 100% money back.
            </p>
          </h2>

          <h1 style={{ marginBottom: "1rem" }}>INSTANT SUPPORT</h1>
          <h2>
            {" "}
            <p
              style={{
                lineHeight: "3rem",
              }}
            >
              We know how important is support for the customer. In case if you
              have any question or problem please contact us. We are fully ready
              for your question or solving the problem.
            </p>
          </h2>
        </div>
      </section>

      <Footer />
    </>
  );
}
