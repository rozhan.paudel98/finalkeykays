import React, { useEffect } from "react";
import Header from "../components/Header";

export default function PaymentSuccess() {
  useEffect(() => {
    localStorage.setItem("cartItems", JSON.stringify([]));
    sessionStorage.setItem("cartItems", JSON.stringify([]));
  }, []);
  return (
    <>
      <Header shouldDisplay={false} />
      <div
        style={{
          height: "100vh",
          alignItems: "center",
        }}
      >
        <h1>Dear User ,</h1>
        <p>
          Payment Successfull ! Check your email after some moment for product
          License.
          <b /> If any issues, contact us through our ticket system.
          <b />
          Thank you for your patience !!
        </p>
      </div>
    </>
  );
}
