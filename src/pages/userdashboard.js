import React, { useEffect } from "react";
import Dashboard from "../components/dashboard";
import Footer from "../components/Footer";
import Header from "../components/Header";


export default function UserDashboard() {
  return (
    <div>
      <Header />
      <Dashboard />
      <Footer/>
    </div>
  );
}
