import Header from "../components/Header";
import Footer from "../components/Footer";
export default function ContactUs() {
  return (
    <>
      <Header />
      <div>
        <section id="aa-catg-head-banner">
          <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img" />
          <div className="aa-catg-head-banner-area">
            <div className="container">
              <div className="aa-catg-head-banner-content">
                <h2>Contact Us</h2>
                <ol className="breadcrumb">
                  <li>
                    <a href="index.html">Home</a>
                  </li>
                  <li className="active">Contact Us</li>
                </ol>
              </div>
            </div>
          </div>
        </section>

        <div
          style={{
            display: " flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            height: "100vh",

            alignItem: "center",
            width: "70vw",
            textAlign: "left",
            marginLeft: "15vw",
            marginBottom: "10vh",
          }}
        >
          <h1>Contact Us</h1> <br />
          <br />
          <br />
          <br />
          <br />
          <h4>
            If you have any questions about our Returns and Refunds Policy,
            please contact us:
            <br />
            <br />
            • By email: service@keykes.com
            <br />
            <br />
            <br />
            Disclaimer: There is no such return policy for cash however in
            return acceptable case we will only return the same value voucher
            code for your next purchase from us. <br />
            <br />
            <br />
            <h4>
              If you have any queries you can visit FAQs (link- frequently asked
              questions) or facing issue for activation of product visit here
              (link-activation guide)
              <br />
              <br />
              <br />
              <br />
              You can contact us in WhatsApp and other social media: (link-
              facebook, instagram) You can issue support ticket from here :
              (link- support ticket)
              <br />
              <br />
              Email us at <a href="mailto:help@keykes.com">
                help@keykes.com
              </a>{" "}
              help@keykes.com <br />
              Happy Shopping ! <br />
            </h4>
          </h4>
        </div>
      </div>
      <Footer />
    </>
  );
}
